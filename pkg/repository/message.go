package repository

import (
	"time"

	"github.com/asdine/storm"
	"github.com/asdine/storm/q"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/entity"
)

type Message interface {
	Save(entity.Message) error
	Get(uuid.UUID) (*entity.Message, error)
	List(int) ([]entity.Message, error)
	OlderThan(time.Time, int) ([]entity.Message, error)
	RefExist(t entity.MessageSourceType, ref string) (bool, error)
}

type message struct {
	l  logrus.FieldLogger
	db *storm.DB
}

func NewMessage(l *logrus.Logger, db *storm.DB) (Message, error) {
	err := db.Init(new(entity.Message))
	if err != nil {
		return nil, err
	}

	return &message{
		l:  l.WithField("Component", "MessageRepository"),
		db: db,
	}, nil
}

func (r *message) Get(id uuid.UUID) (*entity.Message, error) {
	m := new(entity.Message)
	err := r.db.One("ID", id, m)
	return m, err
}

func (r *message) RefExist(t entity.MessageSourceType, ref string) (bool, error) {
	m := []entity.Message{}
	err := r.db.Select(q.And(
		q.Eq("SourceRef", ref),
		q.Eq("SourceType", t),
	)).Find(&m)

	if err == storm.ErrNotFound {
		return false, nil
	}

	if len(m) > 0 {
		return true, err
	}

	return false, err
}

func (r *message) List(limit int) ([]entity.Message, error) {
	all := []entity.Message{}
	err := r.db.AllByIndex("Date", &all, storm.Limit(limit), storm.Reverse())
	return all, err
}

func (r *message) OlderThan(t time.Time, limit int) ([]entity.Message, error) {
	m := []entity.Message{}
	err := r.db.Select(q.Gt("Date", t)).Find(&m)
	return m, err
}

func (r *message) Save(m entity.Message) error {
	return r.db.Save(&m)
}
