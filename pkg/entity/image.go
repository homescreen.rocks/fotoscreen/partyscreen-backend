package entity

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"io"
	"regexp"
	"strings"

	"github.com/google/uuid"
)

var reDataURL = regexp.MustCompile("^data:image/[a-z]+;base64")

type Image struct {
	ID      uuid.UUID
	Content io.Reader
}

func NewImageFromBase64(id uuid.UUID, data string) Image {
	buf := bytes.NewBufferString(data)

	return Image{
		ID:      id,
		Content: base64.NewDecoder(base64.StdEncoding, buf),
	}
}

func NewImageFromDataURL(id uuid.UUID, data string) (*Image, error) {
	parts := strings.SplitN(data, ",", 2)
	if len(parts) != 2 {
		return nil, fmt.Errorf("invalid payload")
	}

	if !reDataURL.MatchString(parts[0]) {
		return nil, fmt.Errorf("invalid payload")
	}

	image := NewImageFromBase64(id, parts[1])

	return &image, nil

}

func (i *Image) Close() error {
	closer, ok := i.Content.(io.Closer)
	if !ok {
		return nil
	}

	return closer.Close()
}
