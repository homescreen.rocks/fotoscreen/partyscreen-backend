package repository

import (
	"io"
	"io/ioutil"
	"os"
	"path"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"

	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/entity"
)

type ImageBuilder struct {
	dataDir     string
	imageSubDir string
}

func NewImageBuilder() *ImageBuilder {
	return &ImageBuilder{
		imageSubDir: "images",
	}

}

func (b *ImageBuilder) WithDataDirectory(dir string) *ImageBuilder {
	b.dataDir = dir
	return b
}

func (b *ImageBuilder) Build(l *logrus.Logger) (Image, error) {
	err := os.MkdirAll(b.dataDir, 0755)
	if err != nil {
		return nil, err
	}

	pictureDir := path.Join(b.dataDir, b.imageSubDir)
	err = os.MkdirAll(pictureDir, 0755)
	if err != nil {
		return nil, err
	}

	return &image{
		l:          l.WithField("Component", "ImageRepository"),
		pictureDir: pictureDir,
	}, nil
}

type Image interface {
	List() ([]entity.Image, error)
	Get(id uuid.UUID) (*entity.Image, error)
	Save(image entity.Image) error
}

type image struct {
	l          logrus.FieldLogger
	pictureDir string
}

func (r *image) List() ([]entity.Image, error) {
	all := []entity.Image{}

	files, err := ioutil.ReadDir(r.pictureDir)
	if err != nil {
		return nil, err
	}

	for _, f := range files {
		logger := r.l.WithFields(logrus.Fields{
			"FileName": f.Name,
		})

		if f.IsDir() {
			logger.Warn("skipping directory")
			continue
		}

		id, err := uuid.Parse(f.Name())
		if err != nil {
			logger.WithField("error", err).
				Warn("skipping file with invalid name")
			continue
		}

		file, err := os.Open(path.Join(r.pictureDir, f.Name()))
		if err != nil {
			return nil, err
		}

		all = append(all, entity.Image{
			ID:      id,
			Content: file,
		})
	}

	return all, nil
}

func (r *image) Get(id uuid.UUID) (*entity.Image, error) {
	file, err := os.Open(path.Join(r.pictureDir, id.String()))
	if err != nil {
		return nil, err
	}

	return &entity.Image{
		ID:      id,
		Content: file,
	}, nil
}

func (r *image) Save(image entity.Image) error {
	filename := path.Join(r.pictureDir, image.ID.String())
	file, err := os.Create(filename)
	if err != nil {
		return err
	}

	_, err = io.Copy(file, image.Content)
	return err
}
