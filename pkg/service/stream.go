package service

import (
	"time"

	"github.com/asdine/storm"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"

	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/entity"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/repository"
	"math/rand"
)

const (
	NewImageEvent  = BroadcastEventType("NewImage")
	TimetableEvent = BroadcastEventType("Timetable")
	InfoEvent      = BroadcastEventType("Info")
)

type Options struct {
	Interval    time.Duration
	MaxBacklog  int
	HistorySize int
}

type Item struct {
	ID  uuid.UUID
	Age time.Duration
}

type Stream interface {
	History() ([]entity.Message, error)
	Run()
}

type stream struct {
	l logrus.FieldLogger
	*Options

	messageRepository repository.Message
	broadcastService  Broadcast

	history []uuid.UUID
	latest  time.Time
}

func NewStream(opts *Options, l *logrus.Logger,
	messageRepository repository.Message,
	broadcastService Broadcast) Stream {
	return &stream{
		l:       l.WithField("Component", "StreamService"),
		Options: opts,

		messageRepository: messageRepository,
		broadcastService:  broadcastService,

		history: []uuid.UUID{},
		latest:  time.Unix(0, 0),
	}
}

func (s *stream) History() ([]entity.Message, error) {
	items := []entity.Message{}

	for _, id := range s.history {
		message, err := s.messageRepository.Get(id)
		if err != nil {
			s.l.WithFields(logrus.Fields{
				"error": err,
				"uuid":  id,
			}).Debugf("unable to get message")
			return nil, err
		}

		items = append(items, *message)
	}

	return items, nil
}

func (s *stream) Run() {
	ticker := time.Tick(s.Options.Interval)

	for {
		err := s.next()
		if err != nil {
			s.l.Error(err)
		}
		<-ticker
	}
}

func (s *stream) next() error {
	l := s.l.WithField("Task", "next")

	messages, err := s.messageRepository.OlderThan(s.latest, 1)
	if err != nil && err != storm.ErrNotFound {
		return err
	}

	if len(messages) > 0 {
		m := messages[0]
		newLatest := m.Date

		l.WithField("NewLatest", newLatest).
			Debug("selecting oldest undisplayed image")
		s.SetImage(m.ID)
		s.latest = newLatest
		return nil
	}

	items, err := s.getItems()
	if err != nil {
		return err
	}

	if len(items) <= 0 {
		l.Warn("skipping, because there are no images yet")
		return nil
	}

	random := rand.Intn(100)
	l.Debug("No new picture, calculating alternatives:" , random, "/100")

	if random < 4 {
		l.Debug("sending InfoEvent")
		s.SetInfo()
	} else if random < 10 {
		l.Debug("sending TimetableEvent")
		s.SetTimetable()
	} else {
		l.Debug("falling back to random image")
		i := rand.Intn(len(items))
		s.SetImage(items[i].ID)

	}

	return nil
}

func (s *stream) getItems() ([]Item, error) {
	messages, err := s.messageRepository.List(s.Options.MaxBacklog)
	if err != nil {
		return nil, err
	}

	items := make([]Item, len(messages))
	now := time.Now()

	for i, message := range messages {
		items[i] = Item{
			ID:  message.ID,
			Age: now.Sub(message.Date),
		}
	}

	return items, nil
}

func (s *stream) SetImage(id uuid.UUID) {
	s.history = append([]uuid.UUID{id}, s.history...)
	if len(s.history) > s.Options.HistorySize {
		s.history = s.history[0:s.Options.HistorySize]
	}

	s.l.WithField("HistorySize", len(s.history)).
		WithField("Newest", id).
		Info("set new image")

	payload, err := s.History()
	if err != nil {
		s.l.Errorf("failed to broadcast new images: %v", err)
		return
	}

	s.broadcastService.Emit(BroadcastEvent{
		Type:    NewImageEvent,
		Payload: payload,
	})
}

func (s *stream) SetTimetable() {
	s.broadcastService.Emit(BroadcastEvent{
		Type:    TimetableEvent,
		Payload: nil,
	})
}
func (s *stream) SetInfo() {
	s.broadcastService.Emit(BroadcastEvent{
		Type:    InfoEvent,
		Payload: "INFO",
	})
}
