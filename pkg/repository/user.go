package repository

import (
	"github.com/asdine/storm"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"

	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/entity"
)

type User interface {
	Save(entity.User) error
	Get(uuid.UUID) (*entity.User, error)
}

type user struct {
	l  logrus.FieldLogger
	db *storm.DB
}

func NewUser(l *logrus.Logger, db *storm.DB) User {
	return &user{
		l:  l.WithField("Component", "UserRepository"),
		db: db,
	}
}

func (r *user) Save(u entity.User) error {
	r.l.WithFields(logrus.Fields{
		"ID":   u.ID,
		"Name": u.Name,
	}).Debug("saving user")
	return r.db.Save(&u)
}

func (r *user) Get(id uuid.UUID) (*entity.User, error) {
	u := new(entity.User)
	err := r.db.One("ID", id, u)
	return u, err
}
