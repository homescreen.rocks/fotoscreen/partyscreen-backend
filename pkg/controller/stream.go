package controller

import (
	"net/http"

	"github.com/sirupsen/logrus"

	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/repository"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/service"
)

type Stream http.Handler

type stream struct {
	l                 logrus.FieldLogger
	streamService     service.Stream
	messageRepository repository.Message
}

func NewStream(l *logrus.Logger, streamService service.Stream, messageRepository repository.Message) Stream {
	return &stream{
		l:                 l.WithField("Component", "StreamController"),
		streamService:     streamService,
		messageRepository: messageRepository,
	}
}

func (c *stream) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	items, err := c.streamService.History()
	if err != nil {
		c.l.WithFields(logrus.Fields{
			"error": err,
		}).Debugf("unable to get message")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp := Messages{}
	resp.Items = items

	renderJSON(w, r, resp)
}
