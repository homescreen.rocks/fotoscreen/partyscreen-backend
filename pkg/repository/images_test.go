package repository_test

import (
	"bytes"
	"testing"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"

	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/entity"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/repository"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/testhelper"
)

func TestImageLifecycle(t *testing.T) {
	dir, deferer := testhelper.TempDir(t, "images")
	defer deferer()

	repo, err := repository.NewImageBuilder().
		WithDataDirectory(dir).
		Build(logrus.StandardLogger())
	testhelper.Must(t, err)

	var (
		redPNG   = "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mOUZGD4DwABbQEafpSlYQAAAABJRU5ErkJggg=="
		greenPNG = "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII="
		//bluePNG  = "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNkYGD6DwABDQEDiBcKPQAAAABJRU5ErkJggg=="
	)

	var (
		pictureID1 = uuid.New()
	)

	t.Run("InitialList", func(t *testing.T) {
		all, err := repo.List()
		testhelper.Must(t, err)

		if len(all) != 0 {
			t.Fatalf("Wrong image count. Have: %d. Want 0.", len(all))
		}
	})

	t.Run("SaveImage1", func(t *testing.T) {
		picture := entity.NewImageFromBase64(pictureID1, redPNG)
		testhelper.Must(t, repo.Save(picture))
	})

	t.Run("ListImage1", func(t *testing.T) {
		all, err := repo.List()
		testhelper.Must(t, err)

		if len(all) != 1 {
			t.Fatalf("Wrong image count. Have: %d. Want 1.", len(all))
		}

		have := all[0]
		if have.ID.ID() != pictureID1.ID() {
			t.Fatalf("Wrong image ID. Have: %s. Want %s.",
				pictureID1.String(), have.ID.String())
		}

		bufHave := new(bytes.Buffer)
		_, err = bufHave.ReadFrom(have.Content)
		testhelper.Must(t, err)

		want := entity.NewImageFromBase64(pictureID1, redPNG)
		bufWant := new(bytes.Buffer)
		_, err = bufWant.ReadFrom(want.Content)
		testhelper.Must(t, err)

		if bytes.Compare(bufHave.Bytes(), bufWant.Bytes()) != 0 {
			t.Fatal("Images do not match")
		}
	})

	t.Run("GetImage1", func(t *testing.T) {
		have, err := repo.Get(pictureID1)
		testhelper.Must(t, err)

		if have.ID.ID() != pictureID1.ID() {
			t.Fatalf("Wrong image ID. Have: %s. Want %s.",
				pictureID1.String(), have.ID.String())
		}

		bufHave := new(bytes.Buffer)
		_, err = bufHave.ReadFrom(have.Content)
		testhelper.Must(t, err)

		want := entity.NewImageFromBase64(pictureID1, redPNG)
		bufWant := new(bytes.Buffer)
		_, err = bufWant.ReadFrom(want.Content)
		testhelper.Must(t, err)
	})

	t.Run("OverwriteImage1", func(t *testing.T) {
		picture := entity.NewImageFromBase64(pictureID1, greenPNG)
		testhelper.Must(t, repo.Save(picture))
	})

	t.Run("ListOverwrittenImage1", func(t *testing.T) {
		all, err := repo.List()
		testhelper.Must(t, err)

		if len(all) != 1 {
			t.Fatalf("Wrong image count. Have: %d. Want 1.", len(all))
		}

		have := all[0]

		bufHave := new(bytes.Buffer)
		_, err = bufHave.ReadFrom(have.Content)
		testhelper.Must(t, err)

		want := entity.NewImageFromBase64(pictureID1, greenPNG)
		bufWant := new(bytes.Buffer)
		_, err = bufWant.ReadFrom(want.Content)
		testhelper.Must(t, err)
	})
}
