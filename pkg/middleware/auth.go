package middleware

import (
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/entity"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/repository"
)

const (
	CookieNameUserID    = "user-id"
	CookieNameUserToken = "user-token"
)

type Auth Interface

type auth struct {
	l              logrus.FieldLogger
	userRepository repository.User
}

func NewAuth(l *logrus.Logger, userRepository repository.User) Auth {
	return &auth{
		l:              l.WithField("Component", "AuthMiddleware"),
		userRepository: userRepository,
	}
}

func (mw *auth) WrapHTTP(next http.Handler, w http.ResponseWriter, req *http.Request) {
	var u *entity.User

	c, err := req.Cookie(CookieNameUserID)
	if err != nil {
		u, err = entity.NewUser()
		if err != nil {
			mw.l.Errorf("failed to create user: %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		err = mw.userRepository.Save(*u)
		if err != nil {
			mw.l.Errorf("failed to save user: %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	} else {
		id, err := uuid.Parse(c.Value)
		if err != nil {
			mw.l.Warnf("failed parse UUID: %v", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		u, err = mw.userRepository.Get(id)
		if err != nil {
			mw.l.Warnf("failed getting user: %v", err)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		mw.l.WithFields(logrus.Fields{
			"ID":   u.ID,
			"Name": u.Name,
		}).Debug("auth: got user")

		c, err := req.Cookie(CookieNameUserToken)
		if err != nil {
			mw.l.Warnf("failed getting user token cookie: %v", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		if c.Value != u.Token {
			mw.l.Warnf("user ID and token do not match")
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
	}

	http.SetCookie(w, &http.Cookie{
		Name:    CookieNameUserID,
		Value:   u.ID.String(),
		Expires: time.Now().Add(365 * 24 * time.Hour),
	})
	http.SetCookie(w, &http.Cookie{
		Name:    CookieNameUserToken,
		Value:   u.Token,
		Expires: time.Now().Add(365 * 24 * time.Hour),
	})

	ctx := req.Context()
	req = req.WithContext(u.NewContext(ctx))

	next.ServeHTTP(w, req)
}
