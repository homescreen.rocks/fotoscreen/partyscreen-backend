package testhelper

import (
	"io/ioutil"
	"os"
	"testing"
)

func TempDir(t testing.TB, prefix string) (string, Deferer) {
	t.Helper()

	const tmp = "./test-tmp"
	Must(t, os.MkdirAll(tmp, 0755))

	path, err := ioutil.TempDir(tmp, prefix)
	Must(t, err)

	return path, func() {
		os.RemoveAll(path)
	}
}
